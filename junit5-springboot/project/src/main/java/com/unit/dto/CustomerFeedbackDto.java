package com.unit.dto;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by ahmed.sheikh.
 */

public class CustomerFeedbackDto  {
    private Long id;

    private String customerName;
    private String feedback;
    private Long rating;
    private String overAllFeedback;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public Long getRating() {
        return rating;
    }

    public void setRating(Long rating) {
        this.rating = rating;
    }

    public String getOverAllFeedback() {
        return overAllFeedback;
    }

    public void setOverAllFeedback(String overAllFeedback) {
        this.overAllFeedback = overAllFeedback;
    }
}