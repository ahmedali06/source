package com.unit;

import com.unit.entity.CustomerFeedback;
import com.unit.repository.CustomerFeedbackRepo;
import com.unit.service.CustomerFeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class ProjectApplication {

	private CustomerFeedbackRepo customerFeedbackRepo;

	@Autowired
	public ProjectApplication(CustomerFeedbackRepo customerFeedbackRepo) {
		this.customerFeedbackRepo = customerFeedbackRepo;
	}

	public static void main(String[] args) {
		SpringApplication.run(ProjectApplication.class, args);
	}

	@PostConstruct
	private void initRecords() {
		CustomerFeedback customerFeedback = new CustomerFeedback();
		customerFeedback.setCustomerName("ahmed test");
		customerFeedback.setFeedback("good");
		customerFeedback.setRating(5l);
		customerFeedback.setOverAllFeedback("good");
		customerFeedbackRepo.save(customerFeedback);
	}
}
