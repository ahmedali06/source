package com.unit.controller;

import com.unit.dto.CustomerFeedbackDto;
import com.unit.service.CustomerFeedbackService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/customer")
public class CustomerController {

    private CustomerFeedbackService customerFeedbackService;

    @Autowired
    public CustomerController(CustomerFeedbackService customerFeedbackService) {
        this.customerFeedbackService = customerFeedbackService;
    }

    private static final Logger log = LoggerFactory.getLogger(CustomerController.class);

    @PostMapping(path = "/save", consumes = "application/json", produces = MediaType.APPLICATION_JSON_VALUE)
    public CustomerFeedbackDto save(@RequestBody CustomerFeedbackDto customer) {
        return customerFeedbackService.createUpdateFeedback(customer);
    }

    @DeleteMapping(path = "/delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> delete(@PathVariable("id") String id) {
        customerFeedbackService.deleteFeedbackById(Long.valueOf(id));
        return new ResponseEntity<>("CustomerFeedback Deleted", HttpStatus.OK);
    }

    @GetMapping(path = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CustomerFeedbackDto>> list() {
        log.info("===========started===============");
        List<CustomerFeedbackDto> customerFeedbackDtos = customerFeedbackService.getAllFeedbacks();
        return new ResponseEntity<>(
                customerFeedbackDtos,
                HttpStatus.OK);

    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public CustomerFeedbackDto get(@PathVariable("id") String id) {
        log.info("===========started===============");
        return customerFeedbackService.getFeedbackById(Long.valueOf(id));
    }

}
