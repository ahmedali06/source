package com.unit.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by ahmed.sheikh.
 */

@Entity
@Table(name = "customer_feedback")
public class CustomerFeedback implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    private String customerName;
    private String feedback;
    private Long rating;
    private String overAllFeedback;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public Long getRating() {
        return rating;
    }

    public void setRating(Long rating) {
        this.rating = rating;
    }

    public String getOverAllFeedback() {
        return overAllFeedback;
    }

    public void setOverAllFeedback(String overAllFeedback) {
        this.overAllFeedback = overAllFeedback;
    }

}