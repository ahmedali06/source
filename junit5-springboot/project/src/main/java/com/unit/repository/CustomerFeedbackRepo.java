package com.unit.repository;

import com.unit.entity.CustomerFeedback;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for CustomerFeedback table operations
 * @author ahmed
 */

@Repository
public interface CustomerFeedbackRepo extends JpaRepository<CustomerFeedback,Long> {

}
