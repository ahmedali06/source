package com.unit.utility;

/**
 * Created by ahmed.sheikh
 */
public class Util {

    public static String getOverallRating(Long rating) {
        String overAllFeedback = "Very Poor";
        if (rating<=1)
            overAllFeedback = "Very Poor";
        else if (rating==2)
            overAllFeedback = "Poor";

        else if (rating==3)
            overAllFeedback = "Good";

        else if (rating==4)
            overAllFeedback = "Very Good";

        else if (rating==5)
            overAllFeedback = "Excellent!";

        return overAllFeedback;
    }
}
