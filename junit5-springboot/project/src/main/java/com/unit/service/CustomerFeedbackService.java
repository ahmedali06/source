package com.unit.service;

import com.unit.dto.CustomerFeedbackDto;
import com.unit.entity.CustomerFeedback;

import java.util.List;

/**
 * Created by ahmed.sheikh on 3/2/2022.
 */
public interface CustomerFeedbackService {

    List<CustomerFeedbackDto> getAllFeedbacks();

    CustomerFeedbackDto getFeedbackById(Long id);

    CustomerFeedbackDto createUpdateFeedback(CustomerFeedbackDto dto);

    CustomerFeedbackDto deleteFeedbackById(Long id);
}
