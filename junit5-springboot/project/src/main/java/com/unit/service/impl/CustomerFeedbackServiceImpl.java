package com.unit.service.impl;

import com.unit.dto.CustomerFeedbackDto;
import com.unit.entity.CustomerFeedback;
import com.unit.repository.CustomerFeedbackRepo;
import com.unit.service.CustomerFeedbackService;
import com.unit.utility.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * This is CustomerFeedbackServiceImpl service class to manage CustomerFeedback
 *
 * @author ahmed
 */

@Service
public class CustomerFeedbackServiceImpl implements CustomerFeedbackService {

    private CustomerFeedbackRepo customerFeedbackRepo;

    @Autowired
    public CustomerFeedbackServiceImpl(CustomerFeedbackRepo customerFeedbackRepo) {
        this.customerFeedbackRepo = customerFeedbackRepo;
    }

    @Override
    public List<CustomerFeedbackDto> getAllFeedbacks() {
        List<CustomerFeedbackDto> dtoList = new ArrayList<>();
        List<CustomerFeedback> list = customerFeedbackRepo.findAll();
        for (CustomerFeedback obj : list) {
            dtoList.add(tranformEntityToDto(obj));
        }
        return dtoList;
    }

    @Override
    public CustomerFeedbackDto getFeedbackById(Long id) {
        if (id == null) {
            return null;
        } else {

            CustomerFeedback obj = customerFeedbackRepo.findById(id).orElse(null);
            if (obj == null) {
                return null;
            } else {
                return tranformEntityToDto(obj);
            }
        }
    }

    @Override
    public CustomerFeedbackDto createUpdateFeedback(CustomerFeedbackDto dto) {
        CustomerFeedback foundResult = null;

        if (dto.getId()!=null) {
            foundResult = customerFeedbackRepo.findById(dto.getId()).orElse(null);

        }

        CustomerFeedback obj = null;
        if (foundResult == null) {
            obj = new CustomerFeedback();
        } else {
            obj = foundResult;
        }

        obj = tranformDtoToEntity(dto, obj);
        obj = customerFeedbackRepo.save(obj);
        dto = tranformEntityToDto(obj);
        return dto;
    }


    @Override
    public CustomerFeedbackDto deleteFeedbackById(Long id) {
        CustomerFeedback customerFeedback = customerFeedbackRepo.findById(id).orElse(null);
        CustomerFeedbackDto dto = tranformEntityToDto(customerFeedback);
        customerFeedbackRepo.delete(customerFeedback);
        return dto;
    }

    private CustomerFeedback tranformDtoToEntity(CustomerFeedbackDto dto, CustomerFeedback obj) {
        obj.setCustomerName(dto.getCustomerName());
        obj.setFeedback(dto.getFeedback());
        obj.setRating(dto.getRating());
        obj.setOverAllFeedback(Util.getOverallRating(dto.getRating()));

        return obj;
    }

    private CustomerFeedbackDto tranformEntityToDto(CustomerFeedback obj) {
        CustomerFeedbackDto dto = new CustomerFeedbackDto();
        dto.setId(obj.getId());
        dto.setCustomerName(obj.getCustomerName());
        dto.setFeedback(obj.getFeedback());
        dto.setRating(obj.getRating());

        dto.setOverAllFeedback(Util.getOverallRating(obj.getRating()));

        return dto;
    }

}
