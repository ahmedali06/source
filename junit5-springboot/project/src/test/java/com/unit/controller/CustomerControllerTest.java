package com.unit.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.unit.ProjectApplication;
import com.unit.entity.CustomerFeedback;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Unit Test cases for CustomerController
 *
 * @author ahmed
 */

@SpringBootTest(classes = ProjectApplication.class)
//@WebAppConfiguration
class CustomerControllerTest  {

    private MockMvc mockMvc;

    @Autowired
    WebApplicationContext webApplicationContext;

    @Autowired
    private ObjectMapper mapper;


    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilter(((request, response, chain) -> {
            response.setCharacterEncoding("UTF-8");
            chain.doFilter(request, response);
        })).build();
    }

    @Test
    void save() throws Exception {
        String uri = "/api/customer/save";

        CustomerFeedback customerFeedback = new CustomerFeedback();
        customerFeedback.setCustomerName("created new test");
        customerFeedback.setFeedback("good");
        customerFeedback.setRating(3l);
        customerFeedback.setOverAllFeedback("good");

        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(customerFeedback);

        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType("application/json").content(requestJson))
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.customerName", is("created new test")));

    }
//
////    @Test
////    void delete() {
////    }
//
    @Test
    void list() throws Exception {
        String uri = "/api/customer/all";
        mockMvc.perform(MockMvcRequestBuilders.get(uri).contentType("application/json"))
                .andDo(print())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());

    }

    @Test
    void get() throws Exception {
        String uri = "/api/customer/1";
        mockMvc.perform(MockMvcRequestBuilders.get(uri).contentType("application/json"))
                .andDo(print())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.customerName", is("ahmed test")));
    }

}