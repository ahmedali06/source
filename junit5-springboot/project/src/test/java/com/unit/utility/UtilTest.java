package com.unit.utility;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by ahmed.sheikh on 3/3/2022.
 */

class UtilTest {

    @Test
    void getOverallRatingWVeryPoor() {
        assertEquals("Poor",Util.getOverallRating(2l));
    }

}