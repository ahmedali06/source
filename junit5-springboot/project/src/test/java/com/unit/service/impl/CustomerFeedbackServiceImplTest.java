package com.unit.service.impl;

import com.unit.dto.CustomerFeedbackDto;
import com.unit.entity.CustomerFeedback;
import com.unit.repository.CustomerFeedbackRepo;
import com.unit.service.CustomerFeedbackService;
import org.aspectj.lang.annotation.After;
import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * Created by ahmed.sheikh on 3/3/2022.
 */
@SpringBootTest
class CustomerFeedbackServiceImplTest {

    @InjectMocks
    private CustomerFeedbackServiceImpl service;

    @Mock
    private CustomerFeedbackRepo customerFeedbackRepo;


    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getAllFeedbacks() {
        when(customerFeedbackRepo.findAll()).thenReturn(listSampleData());
        List<CustomerFeedbackDto> actualResult = service.getAllFeedbacks();
        assertTrue(actualResult.size() > 0, "list not empty");
        assertEquals(2, actualResult.size(), "list size equals");
        verify(customerFeedbackRepo, times(1)).findAll(); // findAll called one time

    }


    @Test
    void getFeedbackById() {
        Long id = 2l;
        CustomerFeedback obj = new CustomerFeedback();
        obj.setId(1l);
        obj.setCustomerName("Test");
        obj.setRating(2l);
        obj.setFeedback("bad");
        obj.setOverAllFeedback("Poor");
        Optional<CustomerFeedback> optionalResult = Optional.of(obj);
        when(customerFeedbackRepo.findById(id)).thenReturn(optionalResult);
        CustomerFeedbackDto actualResult = service.getFeedbackById(id);
        assertEquals(obj.getCustomerName(), actualResult.getCustomerName(), "name equals");
    }

    @Test
    void createUpdateFeedback() {
        CustomerFeedbackDto dto = new CustomerFeedbackDto();

        dto.setCustomerName("Test");
        dto.setRating(2l);
        dto.setFeedback("bad");
        dto.setOverAllFeedback("Poor");

        CustomerFeedback savedObj = new CustomerFeedback();
        savedObj.setId(1l);
        savedObj.setCustomerName("Test");
        savedObj.setRating(2l);
        savedObj.setFeedback("bad");
        savedObj.setOverAllFeedback("Poor");

        Optional<CustomerFeedback> optionalResult = Optional.of(new CustomerFeedback());
        when(customerFeedbackRepo.findById(null)).thenReturn(optionalResult);

        when(customerFeedbackRepo.save(Mockito.any())).thenReturn(savedObj);

        CustomerFeedbackDto actualResult = service.createUpdateFeedback(dto);
        assertEquals(savedObj.getCustomerName(), actualResult.getCustomerName(), "name equals");

    }

    @Test
    void deleteFeedbackById() {
        CustomerFeedbackDto dto = new CustomerFeedbackDto();

        dto.setCustomerName("Test");
        dto.setRating(2l);
        dto.setFeedback("bad");
        dto.setOverAllFeedback("Poor");

        CustomerFeedback obj = new CustomerFeedback();
        obj.setId(1l);
        obj.setCustomerName("Test");
        obj.setRating(2l);
        obj.setFeedback("bad");
        obj.setOverAllFeedback("Poor");

        doNothing().when(customerFeedbackRepo).delete(obj);

        Optional<CustomerFeedback> optionalResult = Optional.of(obj);
        when(customerFeedbackRepo.findById(1l)).thenReturn(optionalResult);

        CustomerFeedbackDto actualResult = service.deleteFeedbackById(1l);
        assertEquals(obj.getCustomerName(), actualResult.getCustomerName(), "name equals");

    }


    private List<CustomerFeedback> listSampleData() {
        List<CustomerFeedback> list = new ArrayList<>();
        CustomerFeedback obj = new CustomerFeedback();
        obj.setId(1l);
        obj.setCustomerName("Test");
        obj.setRating(2l);
        obj.setFeedback("bad");
        obj.setOverAllFeedback("Poor");
        list.add(obj);
        obj = new CustomerFeedback();
        obj.setId(2l);
        obj.setCustomerName("Test");
        obj.setRating(3l);
        obj.setFeedback("good");
        obj.setOverAllFeedback("Good");

        list.add(obj);
        return list;
    }
}